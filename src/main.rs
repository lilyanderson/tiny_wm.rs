fn main() {
    let (connection, screen_num)=xcb::Connection::connect(None).unwrap();
    let screen = connection.get_setup().roots().nth(screen_num as usize).unwrap();
    let mask = xcb::EVENT_MASK_BUTTON_PRESS | xcb::EVENT_MASK_BUTTON_RELEASE | xcb::EVENT_MASK_POINTER_MOTION;
    xcb::grab_button(&connection, true, screen.root(), (mask) as u16,
                     xcb::GRAB_MODE_ASYNC as u8, xcb::NO_SYMBOL as u8, xcb::WINDOW_NONE, 
                     xcb::CURSOR_NONE, xcb::BUTTON_MASK_1 as u8, xcb::MOD_MASK_1 as u16);
    xcb::grab_button(&connection, true, screen.root(), (mask) as u16,
                     xcb::GRAB_MODE_ASYNC as u8, xcb::NO_SYMBOL as u8, xcb::WINDOW_NONE, 
                     xcb::CURSOR_NONE, xcb::BUTTON_MASK_3 as u8, xcb::MOD_MASK_1 as u16);
    let mut btn_info  = (0, 0, 0, None);
    let mut geom = xcb::get_geometry(&connection, screen.root()).get_reply().unwrap();
    loop {
        let event = connection.wait_for_event().unwrap();
        if event.response_type() == xcb::BUTTON_PRESS {
            let button_press:&xcb::ButtonPressEvent = unsafe{ xcb::cast_event(&event) };
            btn_info = (button_press.root_x(), button_press.root_y(), button_press.detail(), Some(button_press.child()));
            geom   = xcb::get_geometry(&connection, btn_info.3.unwrap()).get_reply().unwrap();
        } else if event.response_type() == xcb::MOTION_NOTIFY && btn_info.3 != None {
            let motion_notify : &xcb::MotionNotifyEvent = unsafe {xcb::cast_event(&event)};
            let x_diff = motion_notify.root_x() - btn_info.0;
            let y_diff = motion_notify.root_y() - btn_info.1;
            let list = &if btn_info.2 == 1 {
                [(xcb::CONFIG_WINDOW_X as u16, (geom.x() + x_diff) as u32),
                 (xcb::CONFIG_WINDOW_Y as u16, (geom.y() + y_diff) as u32)]
            } else {
                [(xcb::CONFIG_WINDOW_WIDTH as u16, (geom.width() as i16 + x_diff) as u32),
                 (xcb::CONFIG_WINDOW_HEIGHT as u16, (geom.height() as i16 + y_diff) as u32)]
            };
            xcb::configure_window(&connection, btn_info.3.unwrap(), list);
            connection.flush();
        } else if event.response_type() == xcb::BUTTON_RELEASE {
            btn_info.3 = None;
        }
    }
}
