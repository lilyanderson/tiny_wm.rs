fn main() {
    /* Attempt to connect to the X server. Panic if we cannot. */
    let (connection, screen_num)=xcb::Connection::connect(None).unwrap();
    /* Get a screen object so we can get the root window. Panic if this fails. */
    let screen = connection.get_setup().roots().nth(screen_num as usize).unwrap();
    /* Describe the types of events we are interested in. */
    let mask = xcb::EVENT_MASK_BUTTON_PRESS | xcb::EVENT_MASK_BUTTON_RELEASE | xcb::EVENT_MASK_POINTER_MOTION;
    /* Request to be inforemed of the event types, assuming the alt key is held, and the left mouse
     * button is held */
    xcb::grab_button(&connection, true, screen.root(), (mask) as u16,
                     xcb::GRAB_MODE_ASYNC as u8, xcb::NO_SYMBOL as u8, xcb::WINDOW_NONE, 
                     xcb::CURSOR_NONE, xcb::BUTTON_MASK_1 as u8, xcb::MOD_MASK_1 as u16);
    /* Like above, but with the right mouse button */
    xcb::grab_button(&connection, true, screen.root(), (mask) as u16,
                     xcb::GRAB_MODE_ASYNC as u8, xcb::NO_SYMBOL as u8, xcb::WINDOW_NONE, 
                     xcb::CURSOR_NONE, xcb::BUTTON_MASK_3 as u8, xcb::MOD_MASK_1 as u16);
    /* Holds the x position, y postion, button number and window (in that order)  of a button press
     * event. Useful to save the state of the cursor at the beginning of a move or resize. */
    let mut btn_info  = (0, 0, 0, None);
    /* Used to hold information about the window's state during a move or resize. Initialized with
     * the root window to avoid dealing with options. This also flushes the connection. Without
     * this line, or something else that flushes, a connection.flush() is needed before we can
     * start looking for events. */
    let mut geom = xcb::get_geometry(&connection, screen.root()).get_reply().unwrap();
    /* Event loop */
    loop {
        /* Get an event, panic if something goes wrong */
        let event = connection.wait_for_event().unwrap();
        if event.response_type() == xcb::BUTTON_PRESS {
            /* The library requires that we cast an event into an event type */
            let button_press:&xcb::ButtonPressEvent = unsafe{ xcb::cast_event(&event) };
            /* Store the button state */
            btn_info = (button_press.root_x(), button_press.root_y(), button_press.detail(), Some(button_press.child()));
            /* Store the window state */
            geom   = xcb::get_geometry(&connection, btn_info.3.unwrap()).get_reply().unwrap();
        } else if event.response_type() == xcb::MOTION_NOTIFY && btn_info.3 != None {
            let motion_notify : &xcb::MotionNotifyEvent = unsafe {xcb::cast_event(&event)};
            let x_diff = motion_notify.root_x() - btn_info.0;
            let y_diff = motion_notify.root_y() - btn_info.1;
            let list = &if btn_info.2 == 1 {
                /* This library translates an array of tuples into the bit mask and array of values
                 * that xcb uses as its way of doing things. */
                [(xcb::CONFIG_WINDOW_X as u16, (geom.x() + x_diff) as u32),
                 (xcb::CONFIG_WINDOW_Y as u16, (geom.y() + y_diff) as u32)]
            } else {
                [(xcb::CONFIG_WINDOW_WIDTH as u16, (geom.width() as i16 + x_diff) as u32),
                 (xcb::CONFIG_WINDOW_HEIGHT as u16, (geom.height() as i16 + y_diff) as u32)]
            };
            xcb::configure_window(&connection, btn_info.3.unwrap(), list);
            /* Have to flush the connection to actually make the window resize on the screen. */
            connection.flush();
        } else if event.response_type() == xcb::BUTTON_RELEASE {
            btn_info.3 = None;
        }
    }
}
